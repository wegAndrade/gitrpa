
using GITRPA.Log;
using LibGit2Sharp;
using System.Diagnostics;
using System.Management.Automation;

namespace GITRPA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            folderBrowserDialog.InitialDirectory = "C:";
            txtFolder.Enabled = false;
        }



        private void btnFindFolder_Click(object sender, EventArgs e)
        {
            DialogResult res = folderBrowserDialog.ShowDialog();

            if (res == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Logger.Init();
          
            if (!Directory.Exists(Path.Combine(txtFolder.Text, ".git")))
            {
                MessageBox.Show("A pasta n�o cont�m um reposit�rio Git!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var repoPath = txtFolder.Text;

            try
            {
                using (var repo = new Repository(repoPath))
                {
                    var changes = repo.Diff.Compare<TreeChanges>(repo.Head.Tip.Tree, DiffTargets.Index | DiffTargets.WorkingDirectory);
                    var changeCounts = changes.Count;
                    Logger.Log(@$"Encontradas {changeCounts} altera��es no repo {repoPath}");
                    var branch = repo.Head;
                    Logger.Log($@"Branch: {branch}");
                    int count = 0;
                    foreach (var change in changes)
                    {

                        count++;

                        Logger.Log($@"Altera��o {count}/ {changeCounts}");

                        using (PowerShell powershell = PowerShell.Create())
                        {
                            // this changes from the user folder that PowerShell starts up with to your git repository
                            powershell.AddScript($"cd {repoPath}");

                            Logger.Log($"git add  {change.Path}");

                            powershell.AddScript($"git add  {change.Path}");
                            string commitMessage = GetCommitMessage(change);


                            Logger.Log($"Inicinado Commit {commitMessage}");


                            powershell.AddScript($"git commit -m '{commitMessage}'");


                            powershell.AddScript(@$"git push origin {branch}");

                            Logger.Log($"Inicinado PUSH");

                            var results = await powershell.InvokeAsync();

                            
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            stopwatch.Stop();
            double minutos = stopwatch.Elapsed.TotalMinutes;
            Logger.Log($"Tempo total de processamento {minutos}");
            MessageBox.Show("Sucesso", "Enviado todos os arquivos", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private string GetCommitMessage(TreeEntryChanges change)
        {
            switch (change.Status)
            {
                case ChangeKind.Unmodified:
                    return $"Inalterado - {change.Path}";
                case ChangeKind.Added:
                    return $"Adicionado - {change.Path}";
                case ChangeKind.Deleted:
                    return $"Removido - {change.Path}";
                case ChangeKind.Modified:
                    return $"Modificado - {change.Path}";
                case ChangeKind.Renamed:
                    return $"Renomeado de {change.OldPath} para {change.Path}";
                case ChangeKind.Copied:
                    return $"Copiado de {change.OldPath} para {change.Path}";
                case ChangeKind.Ignored:
                    return $"Ignorado - {change.Path}";
                case ChangeKind.Untracked:
                    return $"N�o rastreado - {change.Path}";
                case ChangeKind.TypeChanged:
                    return $"Tipo alterado - {change.Path}";
                case ChangeKind.Unreadable:
                    return $"N�o leg�vel - {change.Path}";
                case ChangeKind.Conflicted:
                    return $"Em conflito - {change.Path}";
                default:
                    throw new ArgumentException($"Tipo de altera��o desconhecido: {change.Status}");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
